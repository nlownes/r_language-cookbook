#
# Cookbook Name:: r_language
# Recipe:: default
#
# Copyright 2011, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
osx_installer_package 'R-2.14.0.pkg' do
  source 'http://cran.opensourceresources.org/bin/macosx/R-2.14.0.pkg'
  action :install
end
